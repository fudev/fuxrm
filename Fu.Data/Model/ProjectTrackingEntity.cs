﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Fu.Data.Model
{
    [DataContract]
    public class ProjectTrackingEntity
    {
        [DataMember]
        public Guid RecordId { get; set; }
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public float? BirAyKrediMiktari { get; set; }

        [DataMember]
        public Guid? BirAyKrediMiktariParaBirimiId { get; set; }

        [DataMember]
        public string BirAyKrediMiktariParaBirimiIdName { get; set; }

        [DataMember]
        public float? IkiVeSonrakiAyKrediMiktari { get; set; }

        [DataMember]
        public Guid? IkiVeSonrakiAyKrediMiktariParaBirimiId { get; set; }

        [DataMember]
        public string IkiVeSonrakiAyKrediMiktariParaBirimiIdName { get; set; }

        [DataMember]
        public string Aciklama { get; set; }
        [DataMember]
        public string Ada { get; set; }
        [DataMember]
        public string AlbaForm_Id { get; set; }
        [DataMember]
        public string AlbarakaGuid { get; set; }
        [DataMember]
        public string AnaBanka { get; set; }
        [DataMember]
        public Guid? AnaBankaId { get; set; }
        [DataMember]
        public string AnaBankaIdName { get; set; }
        [DataMember]
        public string ArsaPay { get; set; }
        [DataMember]
        public float? ArtirimBedeli { get; set; }
        [DataMember]
        public string ArtirimBedeliYaziIle { get; set; }
        //public Guid? AsamaId { get; set; }
        //public string AsamaIdName { get; set; }
        [DataMember]
        public ProjectTrackingStageEntity Asama { get; set; }
        [DataMember]
        public bool? AsamaIlerlemesin { get; set; }
        [DataMember]
        public Guid? ArtirimBedeliParaBirimiId { get; set; }
        [DataMember]
        public string ArtirimBedeliParaBirimiIdName { get; set; }
        [DataMember]
        public DateTime? AvukatAtamaTarihi { get; set; }
        [DataMember]
        public float? AylikTaksitTutari { get; set; }
        [DataMember]
        public Guid? AylikTaksitTutariParaBirimiId { get; set; }
        [DataMember]
        public string AylikTaksitTutariParaBirimiIdName { get; set; }
        [DataMember]
        public string AylikTaksitTutariYaziIle { get; set; }
        [DataMember]
        public string BagimsizBolumNo { get; set; }
        [DataMember]
        public Guid? BankaId { get; set; }
        [DataMember]
        public string BankaIdName { get; set; }
        [DataMember]
        public Guid? BankaIlgiliKisiId { get; set; }
        [DataMember]
        public string BankaIlgiliKisiIdName { get; set; }
        [DataMember]
        public ContactEntity BankaIlgiliKisi { get; set; }
        [DataMember]
        public string BankaReferansNo { get; set; }




    }
}
