﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Fu.Data.Model
{
    [DataContract]
    public class UserEntity
    {
        public Guid RecordId { get; set; }
        public string UserLogonName { get; set; }
        public string EmailAddress1 { get; set; }
        public string Telephone1 { get; set; }
        public string MobilePhone1 { get; set; }

    }
}
