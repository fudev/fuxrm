﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
 
namespace Fu.Data.Model
{
     [DataContract]
	public class ContactEntity
    {
        [DataMember]
		public Guid RecordId { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string MiddleName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public DateTime? BirthDate { get; set; }
        [DataMember]
        public string TCKN { get; set; }
        [DataMember]
        public string ContactNumber { get; set; }
        [DataMember]
        public string EmailAddress1 { get; set; }
        [DataMember]
        public string EmailAddress2 { get; set; }
        [DataMember]
        public string EmailAddress3 { get; set; }
        [DataMember]
        public string MobilePhone1 { get; set; }
        [DataMember]
        public string MobilePhone2 { get; set; }
        [DataMember]
        public string MobilePhone3 { get; set; }
        [DataMember]
        public string Fax { get; set; }
        [DataMember]
        public string WorkPhone { get; set; }
        [DataMember]
        public string HomePhone { get; set; }
        [DataMember]
        public Guid? ParentCustomerId { get; set; }
        [DataMember]
        public string ParentCustomerName { get; set; }
        [DataMember]
        public int CustomerTypeCode { get; set; }
        [DataMember]
        public string CustomerTypeCodeName { get; set; }
        [DataMember]
        public AccountEntity AccountEntity { get; set; }
	}
}
