﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Fu.Data.Model
{
    /// <summary>
    /// Asama Varligi
    /// </summary>
    [DataContract]
    public class ProjectTrackingStageEntity
    {
        public Guid RecordId { get; set; }
        public string Name { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public UserEntity CreatedBy { get; set; }
        public UserEntity ModifiedBy { get; set; }

    }
}
