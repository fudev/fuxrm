﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fu.Data.Model
{
    public class CountryEntity
    {
        public string RecordId { get; set; }
        public string Name { get; set; }
    }
}
