﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Fu.Data.Model
{
    [DataContract]
    public class AccountEntity
    {
        [DataMember]
        public Guid RecordId { get; set; }
        [DataMember]
        public string AccountName { get; set; }
        [DataMember]
        public string AccountNumber { get; set; }
        [DataMember]
        public string Telephone1 { get; set; }
        [DataMember]
        public string Mobilephone1 { get; set; }
        [DataMember]
        public string EmailAddress1 { get; set; }

        public CityEntity CityEntity { get; set; }
        public TownEntity TownEntity { get; set; }
        public int Code { get; set; }
        public string CourtName { get; set; }
        public bool? RequestsOfficialBond { get; set; }
        public bool? ReportTake { get; set; }
        public CountryEntity CountryEntity { get; set; }
        public bool? CanSendSms { get; set; }
        public bool? RequestEmailPass { get; set; }

    }
}
