﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Fu.Data.Model
{
    [DataContract]
	public class CityEntity
	{
	    [DataMember]
		public Guid RecordId { get; set; }
        [DataMember]
		public string Name { get; set; }
        [DataMember]
		public int? PlateCode { get; set; }

	}
}
