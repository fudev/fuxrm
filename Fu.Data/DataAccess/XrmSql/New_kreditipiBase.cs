//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Fu.Data.DataAccess.XrmSql
{
    using System;
    using System.Collections.Generic;
    
    public partial class New_kreditipiBase
    {
        public New_kreditipiBase()
        {
            this.New_geciciistakibiExtensionBase = new HashSet<New_geciciistakibiExtensionBase>();
            this.New_IstakibiExtensionBase = new HashSet<New_IstakibiExtensionBase>();
            this.New_kredialtturuExtensionBase = new HashSet<New_kredialtturuExtensionBase>();
            this.New_sorumluoldugubkExtensionBase = new HashSet<New_sorumluoldugubkExtensionBase>();
            this.new_systemuser_new_kreditipiBase = new HashSet<new_systemuser_new_kreditipiBase>();
        }
    
        public System.Guid New_kreditipiId { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.Guid> CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<System.Guid> ModifiedBy { get; set; }
        public Nullable<System.Guid> OwningUser { get; set; }
        public Nullable<System.Guid> OwningBusinessUnit { get; set; }
        public int statecode { get; set; }
        public Nullable<int> statuscode { get; set; }
        public Nullable<int> DeletionStateCode { get; set; }
        public byte[] VersionNumber { get; set; }
        public Nullable<int> ImportSequenceNumber { get; set; }
        public Nullable<System.DateTime> OverriddenCreatedOn { get; set; }
        public Nullable<int> TimeZoneRuleVersionNumber { get; set; }
        public Nullable<int> UTCConversionTimeZoneCode { get; set; }
    
        public virtual ICollection<New_geciciistakibiExtensionBase> New_geciciistakibiExtensionBase { get; set; }
        public virtual ICollection<New_IstakibiExtensionBase> New_IstakibiExtensionBase { get; set; }
        public virtual ICollection<New_kredialtturuExtensionBase> New_kredialtturuExtensionBase { get; set; }
        public virtual New_kreditipiExtensionBase New_kreditipiExtensionBase { get; set; }
        public virtual ICollection<New_sorumluoldugubkExtensionBase> New_sorumluoldugubkExtensionBase { get; set; }
        public virtual ICollection<new_systemuser_new_kreditipiBase> new_systemuser_new_kreditipiBase { get; set; }
    }
}
