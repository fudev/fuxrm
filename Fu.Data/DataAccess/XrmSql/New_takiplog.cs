//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Fu.Data.DataAccess.XrmSql
{
    using System;
    using System.Collections.Generic;
    
    public partial class New_takiplog
    {
        public Nullable<int> CreatedByDsc { get; set; }
        public string CreatedByName { get; set; }
        public Nullable<int> ModifiedByDsc { get; set; }
        public string ModifiedByName { get; set; }
        public Nullable<int> New_DisAvukatIdDsc { get; set; }
        public string New_DisAvukatIdName { get; set; }
        public string New_DisAvukatIdYomiName { get; set; }
        public Nullable<int> New_IlgiliKisiDsc { get; set; }
        public string New_IlgiliKisiName { get; set; }
        public string New_IlgiliKisiYomiName { get; set; }
        public Nullable<int> New_IlkAsamaIdDsc { get; set; }
        public string New_IlkAsamaIdName { get; set; }
        public Nullable<int> New_IlkSahipIdDsc { get; set; }
        public string New_IlkSahipIdName { get; set; }
        public string New_IlkSahipIdYomiName { get; set; }
        public Nullable<int> New_IsTakibiIdDsc { get; set; }
        public string New_IsTakibiIdName { get; set; }
        public Nullable<int> New_SonAsamaIdDsc { get; set; }
        public string New_SonAsamaIdName { get; set; }
        public Nullable<int> New_SonSahipIdDsc { get; set; }
        public string New_SonSahipIdName { get; set; }
        public string New_SonSahipIdYomiName { get; set; }
        public Nullable<System.Guid> OwnerId { get; set; }
        public string OwnerIdName { get; set; }
        public string OwnerIdYomiName { get; set; }
        public Nullable<int> OwnerIdDsc { get; set; }
        public Nullable<int> OwnerIdType { get; set; }
        public System.Guid New_takiplogId { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.Guid> CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<System.Guid> ModifiedBy { get; set; }
        public Nullable<System.Guid> OwningUser { get; set; }
        public Nullable<System.Guid> OwningBusinessUnit { get; set; }
        public int statecode { get; set; }
        public Nullable<int> statuscode { get; set; }
        public Nullable<int> DeletionStateCode { get; set; }
        public byte[] VersionNumber { get; set; }
        public Nullable<int> ImportSequenceNumber { get; set; }
        public Nullable<System.DateTime> OverriddenCreatedOn { get; set; }
        public Nullable<int> TimeZoneRuleVersionNumber { get; set; }
        public Nullable<int> UTCConversionTimeZoneCode { get; set; }
        public string New_name { get; set; }
        public Nullable<System.Guid> New_IsTakibiId { get; set; }
        public Nullable<System.Guid> New_IlkSahipId { get; set; }
        public Nullable<System.Guid> New_SonSahipId { get; set; }
        public Nullable<System.Guid> New_IlkAsamaId { get; set; }
        public Nullable<System.Guid> New_SonAsamaId { get; set; }
        public Nullable<System.Guid> New_IlgiliKisi { get; set; }
        public Nullable<System.Guid> New_DisAvukatId { get; set; }
        public string New_Aciklama { get; set; }
    }
}
