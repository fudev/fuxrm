//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Fu.Data.DataAccess.XrmSql
{
    using System;
    using System.Collections.Generic;
    
    public partial class IncidentBase
    {
        public System.Guid IncidentId { get; set; }
        public Nullable<System.Guid> OwningBusinessUnit { get; set; }
        public Nullable<System.Guid> ContractDetailId { get; set; }
        public Nullable<System.Guid> SubjectId { get; set; }
        public Nullable<System.Guid> ContractId { get; set; }
        public Nullable<int> DeletionStateCode { get; set; }
        public Nullable<System.Guid> OwningTeam { get; set; }
        public Nullable<System.Guid> OwningUser { get; set; }
        public Nullable<int> ActualServiceUnits { get; set; }
        public Nullable<int> CaseOriginCode { get; set; }
        public Nullable<int> BilledServiceUnits { get; set; }
        public Nullable<int> CaseTypeCode { get; set; }
        public string ProductSerialNumber { get; set; }
        public string Title { get; set; }
        public Nullable<System.Guid> ProductId { get; set; }
        public Nullable<int> ContractServiceLevelCode { get; set; }
        public Nullable<System.Guid> AccountId { get; set; }
        public string Description { get; set; }
        public Nullable<System.Guid> ContactId { get; set; }
        public Nullable<bool> IsDecrementing { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string TicketNumber { get; set; }
        public Nullable<int> PriorityCode { get; set; }
        public Nullable<int> CustomerSatisfactionCode { get; set; }
        public Nullable<int> IncidentStageCode { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<System.Guid> CreatedBy { get; set; }
        public Nullable<System.DateTime> FollowupBy { get; set; }
        public Nullable<System.Guid> ModifiedBy { get; set; }
        public byte[] VersionNumber { get; set; }
        public int StateCode { get; set; }
        public Nullable<int> SeverityCode { get; set; }
        public Nullable<int> StatusCode { get; set; }
        public Nullable<System.Guid> ResponsibleContactId { get; set; }
        public Nullable<System.Guid> KbArticleId { get; set; }
        public Nullable<int> TimeZoneRuleVersionNumber { get; set; }
        public Nullable<int> ImportSequenceNumber { get; set; }
        public Nullable<int> UTCConversionTimeZoneCode { get; set; }
        public Nullable<System.DateTime> OverriddenCreatedOn { get; set; }
    
        public virtual AccountBase AccountBase { get; set; }
        public virtual ContactBase ContactBase { get; set; }
        public virtual ContactBase ContactBase1 { get; set; }
    }
}
