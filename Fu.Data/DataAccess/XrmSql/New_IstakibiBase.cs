//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Fu.Data.DataAccess.XrmSql
{
    using System;
    using System.Collections.Generic;
    
    public partial class New_IstakibiBase
    {
        public New_IstakibiBase()
        {
            this.New_bankaaciklamaExtensionBase = new HashSet<New_bankaaciklamaExtensionBase>();
            this.New_CagriKayitlariExtensionBase = new HashSet<New_CagriKayitlariExtensionBase>();
            this.New_dialogExtensionBase = new HashSet<New_dialogExtensionBase>();
            this.New_evrakExtensionBase = new HashSet<New_evrakExtensionBase>();
            this.New_loggeciciistakipExtensionBase = new HashSet<New_loggeciciistakipExtensionBase>();
            this.New_istakiphataExtensionBase = new HashSet<New_istakiphataExtensionBase>();
            this.New_mesajlarExtensionBase = new HashSet<New_mesajlarExtensionBase>();
            this.New_takiplogExtensionBase = new HashSet<New_takiplogExtensionBase>();
        }
    
        public System.Guid New_IstakibiId { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.Guid> CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<System.Guid> ModifiedBy { get; set; }
        public Nullable<System.Guid> OwningUser { get; set; }
        public Nullable<System.Guid> OwningBusinessUnit { get; set; }
        public int statecode { get; set; }
        public Nullable<int> statuscode { get; set; }
        public Nullable<int> DeletionStateCode { get; set; }
        public byte[] VersionNumber { get; set; }
        public Nullable<int> ImportSequenceNumber { get; set; }
        public Nullable<System.DateTime> OverriddenCreatedOn { get; set; }
        public Nullable<int> TimeZoneRuleVersionNumber { get; set; }
        public Nullable<int> UTCConversionTimeZoneCode { get; set; }
        public Nullable<System.Guid> TransactionCurrencyId { get; set; }
        public Nullable<decimal> ExchangeRate { get; set; }
    
        public virtual ICollection<New_bankaaciklamaExtensionBase> New_bankaaciklamaExtensionBase { get; set; }
        public virtual ICollection<New_CagriKayitlariExtensionBase> New_CagriKayitlariExtensionBase { get; set; }
        public virtual ICollection<New_dialogExtensionBase> New_dialogExtensionBase { get; set; }
        public virtual ICollection<New_evrakExtensionBase> New_evrakExtensionBase { get; set; }
        public virtual New_IstakibiExtensionBase New_IstakibiExtensionBase { get; set; }
        public virtual ICollection<New_loggeciciistakipExtensionBase> New_loggeciciistakipExtensionBase { get; set; }
        public virtual ICollection<New_istakiphataExtensionBase> New_istakiphataExtensionBase { get; set; }
        public virtual ICollection<New_mesajlarExtensionBase> New_mesajlarExtensionBase { get; set; }
        public virtual ICollection<New_takiplogExtensionBase> New_takiplogExtensionBase { get; set; }
    }
}
