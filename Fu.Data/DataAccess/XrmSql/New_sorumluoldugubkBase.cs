//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Fu.Data.DataAccess.XrmSql
{
    using System;
    using System.Collections.Generic;
    
    public partial class New_sorumluoldugubkBase
    {
        public New_sorumluoldugubkBase()
        {
            this.new_systemuser_new_sorumluoldugubkBase = new HashSet<new_systemuser_new_sorumluoldugubkBase>();
        }
    
        public System.Guid New_sorumluoldugubkId { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.Guid> CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<System.Guid> ModifiedBy { get; set; }
        public Nullable<System.Guid> OrganizationId { get; set; }
        public int statecode { get; set; }
        public Nullable<int> statuscode { get; set; }
        public Nullable<int> DeletionStateCode { get; set; }
        public byte[] VersionNumber { get; set; }
        public Nullable<int> ImportSequenceNumber { get; set; }
        public Nullable<System.DateTime> OverriddenCreatedOn { get; set; }
        public Nullable<int> TimeZoneRuleVersionNumber { get; set; }
        public Nullable<int> UTCConversionTimeZoneCode { get; set; }
    
        public virtual New_sorumluoldugubkExtensionBase New_sorumluoldugubkExtensionBase { get; set; }
        public virtual ICollection<new_systemuser_new_sorumluoldugubkBase> new_systemuser_new_sorumluoldugubkBase { get; set; }
    }
}
