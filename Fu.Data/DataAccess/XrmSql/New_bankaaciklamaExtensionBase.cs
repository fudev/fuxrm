//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Fu.Data.DataAccess.XrmSql
{
    using System;
    using System.Collections.Generic;
    
    public partial class New_bankaaciklamaExtensionBase
    {
        public System.Guid New_bankaaciklamaId { get; set; }
        public string New_name { get; set; }
        public Nullable<System.Guid> New_istakibiId { get; set; }
        public Nullable<System.Guid> New_geciciistakibiId { get; set; }
        public string New_aciklama { get; set; }
    
        public virtual New_bankaaciklamaBase New_bankaaciklamaBase { get; set; }
        public virtual New_geciciistakibiBase New_geciciistakibiBase { get; set; }
        public virtual New_IstakibiBase New_IstakibiBase { get; set; }
    }
}
