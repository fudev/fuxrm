//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Fu.Data.DataAccess.XrmSql
{
    using System;
    using System.Collections.Generic;
    
    public partial class AccountExtensionBase
    {
        public System.Guid AccountId { get; set; }
        public Nullable<bool> New_ResmiSenetIstiyorMu { get; set; }
        public Nullable<System.Guid> New_bankasehirid { get; set; }
        public Nullable<System.Guid> New_bankailceid { get; set; }
        public Nullable<System.Guid> New_bankaulkeid { get; set; }
        public Nullable<bool> New_emailsifresorulsunmu { get; set; }
        public Nullable<int> New_Kod { get; set; }
        public string New_MahkemeAd { get; set; }
        public Nullable<bool> New_raporaalma { get; set; }
        public Nullable<bool> New_cansendsms { get; set; }
        public string New_WebSitesiAd { get; set; }
    
        public virtual New_cityBase New_cityBase { get; set; }
        public virtual New_countryBase New_countryBase { get; set; }
        public virtual New_countyBase New_countyBase { get; set; }
    }
}
