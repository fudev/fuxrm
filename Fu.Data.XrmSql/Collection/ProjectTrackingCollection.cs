﻿using Fu.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fu.Data.XrmSql.Collection
{
   public class ProjectTrackingCollection
    {
       public ProjectTrackingEntity Get_ByRefNo(string refNo)
       {
           using (var sql = new Fu.Data.DataAccess.XrmSql.FuXrmSqlEntities())
           {

               var result = from r in sql.New_Istakibi
                            // where r.New_AlbaFormId == refNo
                            select new ProjectTrackingEntity
                            {
                                Aciklama = r.New_aciklama,
                                Ada = r.New_Ada,
                                AlbaForm_Id = r.New_AlbaFormId,
                                BankaIlgiliKisi = (from c in sql.Contact
                                                   where c.ContactId == r.New_BankailgiliKisisiId
                                                   select new ContactEntity
                                                   {
                                                       RecordId = c.ContactId,
                                                       EmailAddress1 = c.EMailAddress1,
                                                       MobilePhone1 = c.MobilePhone,
                                                       FirstName = c.FirstName,
                                                       LastName = c.LastName,
                                                       FullName = c.FullName
                                                   }).FirstOrDefault()
                            };

               return result.FirstOrDefault();
           }

       }
    }
}
