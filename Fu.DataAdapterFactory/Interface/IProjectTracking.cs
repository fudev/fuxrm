﻿using Fu.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fu.DataAdapterFactory.Interface
{
    public interface IProjectTracking
    {
        ProjectTrackingEntity Get_ByRefNo(string refNo);
        ProjectTrackingEntity Get_ById(Guid recordId);
        List<ProjectTrackingEntity> Get_ByStage(Guid projectTrackingStageRecordId);
    }
}
