﻿using Fu.Data.Model;
using Fu.DataAdapterFactory.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Fu.DataAdapterFactory.Service
{

	public class ProjectTrackingService : IProjectTracking
	{
		private readonly Fu.Data.XrmSql.Collection.ProjectTrackingCollection _sqlProjectTrackingCollection;
		public ProjectTrackingService()
		{
			_sqlProjectTrackingCollection = new Data.XrmSql.Collection.ProjectTrackingCollection();
		}
		public Data.Model.ProjectTrackingEntity Get_ByRefNo(string refNo)
		{
			return _sqlProjectTrackingCollection.Get_ByRefNo(refNo);
		}

		public Data.Model.ProjectTrackingEntity Get_ById(Guid recordId)
		{
			throw new NotImplementedException();
		}

		public List<Data.Model.ProjectTrackingEntity> Get_ByStage(Guid projectTrackingStageRecordId)
		{
			throw new NotImplementedException();
		}
	}
}
