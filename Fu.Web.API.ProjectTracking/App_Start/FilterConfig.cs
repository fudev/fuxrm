﻿using System.Web;
using System.Web.Mvc;

namespace Fu.Web.API.ProjectTracking
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
