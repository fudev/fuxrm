﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace testet
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Fu.DataAdapterFactory.Service.ProjectTrackingService ptService = new Fu.DataAdapterFactory.Service.ProjectTrackingService();
            var result = ptService.Get_ByRefNo("");

            string serializedResult = JsonConvert.SerializeObject(result);
            richTextBox1.AppendText(serializedResult);
        }
    }
}
